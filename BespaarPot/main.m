//
//  main.m
//  BespaarPot
//
//  Created by FHICT on 10/9/13.
//  Copyright (c) 2013 FHICT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BPAppDelegate class]));
    }
}
