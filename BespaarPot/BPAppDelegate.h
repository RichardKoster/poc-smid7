//
//  BPAppDelegate.h
//  BespaarPot
//
//  Created by FHICT on 10/9/13.
//  Copyright (c) 2013 FHICT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
