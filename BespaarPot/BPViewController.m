//
//  BPViewController.m
//  BespaarPot
//
//  Created by FHICT on 10/9/13.
//  Copyright (c) 2013 FHICT. All rights reserved.
//

#import "BPViewController.h"
#import "BPAddNewItemCell.h"
#import "BPItemCell.h"

@interface BPViewController (){
    NSMutableArray *ARR_boodschappenlijst;
    BOOL afgescheurd;
    BOOL released;
    int startingId;
    int endingId;
    CGRect topBoodschappenlijstRandStartPositie;
    CGRect topBoodschappenlijstScheurRandStartPositie;
    CGRect boodschappenlijstStartPositie;
    UIPanGestureRecognizer *recognizer;
    UITableViewCell *tempCell;
    CGFloat piggyStartPosition;
}

@end

@implementation BPViewController
@synthesize TV_boodschappenlijst, IMG_topBoodschappenlijstScheurRand, IMG_topBoodschappenlijstRand, IMG_absoluteTopBoodschappenlijst, IMG_piggy;

- (void)viewDidLoad
{
    [super viewDidLoad];
    TV_boodschappenlijst.backgroundColor = [UIColor clearColor];
    TV_boodschappenlijst.opaque = NO;
    TV_boodschappenlijst.backgroundView = nil;
    ARR_boodschappenlijst = [[NSMutableArray alloc] init];
    TV_boodschappenlijst.contentInset = UIEdgeInsetsMake(0, 0, 460, 0);
    afgescheurd = false;
    released = false;
	// Do any additional setup after loading the view, typically from a nib.
    startingId = -1;
    endingId = -1;
    topBoodschappenlijstRandStartPositie = IMG_topBoodschappenlijstRand.frame;
    topBoodschappenlijstScheurRandStartPositie = IMG_topBoodschappenlijstScheurRand.frame;
    boodschappenlijstStartPositie = TV_boodschappenlijst.frame;
    recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(recognizePan:)];
    recognizer.delegate = self;
    [TV_boodschappenlijst addGestureRecognizer:recognizer];
    piggyStartPosition = IMG_piggy.frame.origin.y;
    [IMG_piggy removeConstraints:IMG_piggy.constraints];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [ARR_boodschappenlijst count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier;
    if(indexPath.row < [ARR_boodschappenlijst count])
    {
        CellIdentifier = @"listItem";
        BPItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = nil;
        cell.cellText.center = CGPointMake(130, 22);
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.cellText setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else{
        CellIdentifier = @"addItem";
        BPAddNewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        cell.backgroundView = nil;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == [ARR_boodschappenlijst count])
    {
        [TV_boodschappenlijst deselectRowAtIndexPath:indexPath animated:YES];
        NSMutableArray *listItem = [[NSMutableArray alloc] init];
        [ARR_boodschappenlijst addObject:listItem];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:([ARR_boodschappenlijst count] -1) inSection:0];
        [TV_boodschappenlijst insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = IMG_piggy.frame;
            frame.origin.y += 22;
            IMG_piggy.frame = frame;
        } completion:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == [ARR_boodschappenlijst count])
    {
        return NO;
    }
    else{
        return YES;
    }
}

-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    released = false;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if(!afgescheurd)
    {
        CGRect topScheurImageFrame = IMG_topBoodschappenlijstScheurRand.frame;
        CGRect topImageFrame = IMG_topBoodschappenlijstRand.frame;
        CGRect absoluteTopFrame = IMG_absoluteTopBoodschappenlijst.frame;
        if(-1*scrollView.contentOffset.y-62 >= 0)
        {
            absoluteTopFrame.size.height = -1*scrollView.contentOffset.y-62;
        }
        topScheurImageFrame.origin.y = -1*scrollView.contentOffset.y-62;
        topImageFrame.origin.y = -1*scrollView.contentOffset.y-16;
        
        if(-1*scrollView.contentOffset.y-64 < 80){
            afgescheurd = false;
            [IMG_topBoodschappenlijstScheurRand setImage:[UIImage imageNamed:@"top0.png"]];
        }
        else if(-1*scrollView.contentOffset.y-64 >= 80 && -1*scrollView.contentOffset.y-64 < 85 && !released)
        {
            afgescheurd = false;
            [IMG_topBoodschappenlijstScheurRand setImage:[UIImage imageNamed:@"top1.png"]];
        }
        else if(-1*scrollView.contentOffset.y-64 >= 85 && -1*scrollView.contentOffset.y-64 < 90 && !released)
        {
            afgescheurd = false;
            [IMG_topBoodschappenlijstScheurRand setImage:[UIImage imageNamed:@"top2.png"]];
        }
        else if(-1*scrollView.contentOffset.y-64 >= 90 && -1*scrollView.contentOffset.y-64 < 95 && !released)
        {
            afgescheurd = false;
            [IMG_topBoodschappenlijstScheurRand setImage:[UIImage imageNamed:@"top3.png"]];
        }
        else if(-1*scrollView.contentOffset.y-64 >= 95 && !released)
        {
            [IMG_topBoodschappenlijstScheurRand setImage:[UIImage imageNamed:@"top4.png"]];
            topScheurImageFrame.origin.y = 97;
            topImageFrame.origin.y = -1*scrollView.contentOffset.y-16;
            absoluteTopFrame.size.height = 120;
        }
        IMG_topBoodschappenlijstScheurRand.frame = topScheurImageFrame;
        IMG_topBoodschappenlijstRand.frame = topImageFrame;
        IMG_absoluteTopBoodschappenlijst.frame = absoluteTopFrame;
        UITableViewCell *testCell = [TV_boodschappenlijst cellForRowAtIndexPath:[NSIndexPath indexPathForItem:[ARR_boodschappenlijst count] inSection:0]];
        CGFloat relativeCellPosition = testCell.frame.origin.y - TV_boodschappenlijst.contentOffset.y-64;
        NSLog(@"%f", relativeCellPosition);
        CGRect piggyFrame = IMG_piggy.frame;
        piggyFrame.origin.y = (piggyStartPosition+relativeCellPosition/2.2);
        if((piggyStartPosition+relativeCellPosition/2.2) > piggyStartPosition)
        {
            IMG_piggy.frame = piggyFrame;
        }
    }
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    released = true;
    if(-1*scrollView.contentOffset.y-64 >= 95)
    {
        afgescheurd = true;
    }
    if(afgescheurd){
        [TV_boodschappenlijst setUserInteractionEnabled:NO];
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame1 = TV_boodschappenlijst.frame;
            CGRect frame2 = IMG_topBoodschappenlijstRand.frame;
            CGRect frame3 = IMG_topBoodschappenlijstScheurRand.frame;
            CGRect frame4 = IMG_absoluteTopBoodschappenlijst.frame;
            frame1.origin.y = self.view.frame.size.height;
            frame2.origin.y = self.view.frame.size.height+56;
            frame3.origin.y = frame3.size.height-15;
            frame4.size.height = 62;
            TV_boodschappenlijst.frame = frame1;
            IMG_topBoodschappenlijstRand.frame = frame2;
            IMG_topBoodschappenlijstScheurRand.frame = frame3;
            IMG_absoluteTopBoodschappenlijst.frame = frame4;
        }completion:^(BOOL finished) {
            CGRect frame1 = TV_boodschappenlijst.frame;
            frame1.origin.y = -36;
            TV_boodschappenlijst.frame = frame1;
            IMG_topBoodschappenlijstRand.frame = topBoodschappenlijstRandStartPositie;
            [UIView animateWithDuration:0.3 animations:^{
                TV_boodschappenlijst.frame = boodschappenlijstStartPositie;
            }completion:^(BOOL complete) {
                if(complete)
                {
                    [TV_boodschappenlijst setUserInteractionEnabled:YES];
                    afgescheurd = false;
                }
            }];
            IMG_topBoodschappenlijstRand.frame = topBoodschappenlijstRandStartPositie;
            IMG_topBoodschappenlijstScheurRand.frame = topBoodschappenlijstScheurRandStartPositie;
            [ARR_boodschappenlijst removeAllObjects];
            
            [TV_boodschappenlijst reloadData];
        }];
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

- (void)recognizePan:(UIPanGestureRecognizer *)sender {
    tempCell = [TV_boodschappenlijst cellForRowAtIndexPath:[TV_boodschappenlijst indexPathForRowAtPoint:[sender locationInView:sender.view]]];
    
    if([TV_boodschappenlijst indexPathForCell:tempCell].row != [ARR_boodschappenlijst count]){
        BPItemCell *cell = (BPItemCell*)[TV_boodschappenlijst cellForRowAtIndexPath:[TV_boodschappenlijst indexPathForRowAtPoint:[sender locationInView:sender.view]]];
        NSLog(@"%f",cell.cellText.center.x);
        if(sender.state == UIGestureRecognizerStateChanged)
        {
            CGPoint translation = [recognizer translationInView:cell.cellText];
            if(cell.cellText.center.x + translation.x <= 50)
            {
                cell.cellText.center = CGPointMake(50, 22);
            }
            else if(cell.cellText.center.x + translation.x >= 130)
            {
                cell.cellText.center = CGPointMake(130, 22);
            }
            else{
                cell.cellText.center = CGPointMake(cell.cellText.center.x + translation.x, cell.cellText.center.y);
                [recognizer setTranslation:CGPointMake(0, 0) inView:cell.cellText];
            }
        }
        else if(sender.state == UIGestureRecognizerStateEnded)
        {
            if(cell.cellText.center.x < 51 && cell != nil){
                [TV_boodschappenlijst beginUpdates];
                [ARR_boodschappenlijst removeObjectAtIndex:[TV_boodschappenlijst indexPathForCell:cell].row];
                // Delete the row from the data source
                [TV_boodschappenlijst deleteRowsAtIndexPaths:[NSArray arrayWithObjects:[TV_boodschappenlijst indexPathForCell:cell], nil] withRowAnimation:UITableViewRowAnimationMiddle ];
                [TV_boodschappenlijst endUpdates];
                double delayInSeconds = 0.4;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [TV_boodschappenlijst reloadData];
                });
            }
            else{
                [UIView animateWithDuration:0.3 animations:^{
                    cell.cellText.center = CGPointMake(130, 22);
                } completion:^(BOOL finished) {
                    
                }];
            }
        }
        else if(sender.state == UIGestureRecognizerStateCancelled){
            NSLog(@"kkk");
        }
        [cell setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:(-1*(cell.cellText.center.x-50)/80)+0.4]];
        [cell.cellText setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:(-1*(cell.cellText.center.x-50)/80)+0.4]];
    }

}

-(BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer{
    CGPoint translation = [gestureRecognizer translationInView:[gestureRecognizer.view superview]];
    
    // Check for horizontal gesture
    NSLog(@"%f",translation.x);
    if (fabsf(translation.x) > fabsf(translation.y))
    {
        return YES;
    }
    
    return NO;
}
@end
