//
//  BPViewController.h
//  BespaarPot
//
//  Created by FHICT on 10/9/13.
//  Copyright (c) 2013 FHICT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *TV_boodschappenlijst;
@property (weak, nonatomic) IBOutlet UIImageView *IMG_topBoodschappenlijstScheurRand;
@property (weak, nonatomic) IBOutlet UIImageView *IMG_topBoodschappenlijstRand;
@property (weak, nonatomic) IBOutlet UIImageView *IMG_absoluteTopBoodschappenlijst;
@property (weak, nonatomic) IBOutlet UIImageView *IMG_piggy;

@end
