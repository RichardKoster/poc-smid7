//
//  BPItemCell.h
//  BespaarPot
//
//  Created by FHICT on 10/9/13.
//  Copyright (c) 2013 FHICT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BPItemCell : UITableViewCell <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *cellText;

@end
